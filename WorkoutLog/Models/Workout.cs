﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkoutLog.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public DateTimeOffset Date { get; set; }
        public int Distance { get; set; }
        public long TimeInSec { get; set; }
    }
}
